package com.sisisoso.wholesaleairtimes.app.api.utils

import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns

abstract class TextValidator : TextWatcher {
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun afterTextChanged(s: Editable) {
        validate(s.toString())
    }

    abstract fun validate(s: String?)

    companion object {
        fun isEmpty(text: String?): Boolean {
            return text == null || text.isEmpty()
        }

        fun isEmptyOrSpace(input: String?): Boolean {
            return input == null || isEmpty(input.trim { it <= ' ' })
        }

        fun isPhoneNumber(text: String?): Boolean {
            return text != null && Patterns.PHONE.matcher(text).matches()
        }



        fun isLongerThan(text: String?, length: Int): Boolean {
            return text != null && text.length > length
        }

        fun isEmail(text: String?): Boolean {
            return text != null && Patterns.EMAIL_ADDRESS.matcher(text).matches()
        }

        fun isNationalID(text: String?): Boolean {
            return text != null && text.length == 7 || text != null && text.length == 8 || text != null && text.length == 10
        }
    }
}