package com.sisisoso.wholesaleairtimes.app.api

import okhttp3.Headers
import retrofit2.Response
import java.io.IOException

class APIResponse<T>(response: Response<T>?) {
    private var body: T? = null
    private var headers: Headers? = null
    private var errorBody: String? = null
    private var code = 0
    var isSuccessful = false

    init {
        if (response != null) {
            body = response.body()
            code = response.code()
            isSuccessful = response.isSuccessful
            headers = response.headers()
            errorBody = try {
                response.errorBody()!!.string()
            } catch (e: IOException) {
                e.printStackTrace()
                null
            } catch (e: NullPointerException) {
                e.printStackTrace()
                null
            }
        } else {
            body = null
            this.code = 0
            isSuccessful = false
            headers = null
            errorBody = "No connection to our server"
        }
    }

    fun headers(): Headers? {
        return headers
    }

    fun body(): T? {
        return body
    }

    fun code(): Int {
        return code
    }

    fun errorBody(): String? {
        return errorBody
    }

    fun serverError(): Boolean {
        return code >= 500
    }

    fun badRequest(): Boolean {
        return code == 400
    }

    fun authenticationError(): Boolean {
        return code == 401
    }

    fun authorizationError(): Boolean {
        return code == 403
    }

}