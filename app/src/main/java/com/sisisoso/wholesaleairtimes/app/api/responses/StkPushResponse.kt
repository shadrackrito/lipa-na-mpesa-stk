package com.sisisoso.wholesaleairtimes.app.api.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class StkPushResponse : Serializable {
    @SerializedName("MerchantRequestID")
    var MerchantRequestID: String? = null

    @SerializedName("CheckoutRequestID")
    var CheckoutRequestID: String? = null

    @SerializedName("ResponseCode")
    var ResponseCode: String? = null

    @SerializedName("ResponseDescription")
    var ResponseDescription: String? = null

    @SerializedName("CustomerMessage")
    var CustomerMessage: String? = null
}