package com.sisisoso.wholesaleairtimes.app.api.interceptor

import android.util.Base64
import com.sisisoso.wholesaleairtimes.app.api.utils.AppConstants
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

class AccessTokenInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val keys: String = AppConstants.CONSUMER_KEY + ":" + AppConstants.CONSUMER_SECRET
        val request: Request = chain.request().newBuilder()
            .addHeader(
                "Authorization",
                "Basic " + Base64.encodeToString(keys.toByteArray(), Base64.NO_WRAP)
            )
            .build()
        return chain.proceed(request)
    }
}