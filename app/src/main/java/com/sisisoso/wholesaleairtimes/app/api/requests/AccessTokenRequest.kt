package com.sisisoso.wholesaleairtimes.app.api.requests

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AccessTokenRequest : Serializable {
    @SerializedName("api_key")
    var api_key: String? = null

    @SerializedName("account")
    var account: String? = null

    @SerializedName("request_type")
    var requestType: String? = null
}