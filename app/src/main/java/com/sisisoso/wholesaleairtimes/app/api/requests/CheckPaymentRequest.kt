package com.sisisoso.wholesaleairtimes.app.api.requests

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CheckPaymentRequest : Serializable {
    @SerializedName("MerchantRequestID")
    var MerchantRequestID: String? = null

    @SerializedName("CheckoutRequestID")
    var CheckoutRequestID: String? = null

    @SerializedName("BusinessShortCode")
    var BusinessShortCode: String? = null

    @SerializedName("Password")
    var Password: String? = null

    @SerializedName("Timestamp")
    var Timestamp: String? = null
}