package com.sisisoso.wholesaleairtimes.app.api.requests

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class StkPushRequest : Serializable {
    @SerializedName("telephone")
    var telephone: String? = null

    @SerializedName("account_no")
    var accountNo: String? = null

    @SerializedName("amount_to_pay")
    var amountToPay: Int? = null
}