package com.sisisoso.wholesaleairtimes.app.api

import com.sisisoso.wholesaleairtimes.app.api.utils.AppConstants.BASE_URL
import com.sisisoso.wholesaleairtimes.app.api.utils.AppConstants.CONNECT_TIMEOUT
import com.sisisoso.wholesaleairtimes.app.api.utils.AppConstants.READ_TIMEOUT
import com.sisisoso.wholesaleairtimes.app.api.utils.AppConstants.WRITE_TIMEOUT
import com.sisisoso.wholesaleairtimes.app.api.interceptor.AccessTokenInterceptor
import com.sisisoso.wholesaleairtimes.app.api.interceptor.AuthInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiClient {
    private lateinit var retrofit: Retrofit
    private var isDebug = false
    private var isGetAccessToken = false
    private var mAuthToken: String? = null
    private val httpLoggingInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor()

    /**
     * Set the [Retrofit] log level. This allows one to view network traffic.
     *
     * @param isDebug If true, the log level is set to
     * [HttpLoggingInterceptor.Level.BODY]. Otherwise
     * [HttpLoggingInterceptor.Level.NONE].
     */
    fun setIsDebug(isDebug: Boolean): ApiClient {
        this.isDebug = isDebug
        return this
    }

    /**
     * Helper method used to set the authenication Token
     *
     * @param authToken token from api
     */
    fun setAuthToken(authToken: String): ApiClient {
        mAuthToken = authToken
        return this
    }

    /**
     * Helper method used to determine if get token enpoint has been invoked. This should be called
     * only when requesting of an accessToken
     *
     * @param getAccessToken [Boolean]
     */
    fun setGetAccessToken(getAccessToken: Boolean): ApiClient {
        isGetAccessToken = getAccessToken
        return this
    }

    /**
     * Configure OkHttpClient
     *
     * @return OkHttpClient
     */
    private fun okHttpClient(): OkHttpClient.Builder {
        val okHttpClient = OkHttpClient.Builder()
        okHttpClient
            .connectTimeout(CONNECT_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor)
        return okHttpClient
    }

    /**
     * Return the current [Retrofit] instance. If none exists (first call, API key changed),
     * builds a new one.
     *
     *
     * When building, sets the endpoint and a [HttpLoggingInterceptor] which adds the API key as query param.
     */
    private val restAdapter: Retrofit
        private get() {
            val builder: Retrofit.Builder = Retrofit.Builder()
            builder.baseUrl(BASE_URL)
            builder.addConverterFactory(GsonConverterFactory.create())
            if (isDebug) {
                httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            }
            val okhttpBuilder: OkHttpClient.Builder = okHttpClient()
            if (isGetAccessToken) {
                okhttpBuilder.addInterceptor(AccessTokenInterceptor())
            }
            if (mAuthToken != null && mAuthToken!!.isNotEmpty()) {
                okhttpBuilder.addInterceptor(AuthInterceptor(mAuthToken!!))
            }
            builder.client(okhttpBuilder.build())
            retrofit = builder.build()
            return retrofit
        }

    /**
     * Create service instance.
     *
     * @return STKPushService Service.
     */
    fun mpesaService(): RestApi {
        return restAdapter.create<RestApi>(RestApi::class.java)
    }
}