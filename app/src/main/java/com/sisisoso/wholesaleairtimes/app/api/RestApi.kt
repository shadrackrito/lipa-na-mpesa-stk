package com.sisisoso.wholesaleairtimes.app.api

import com.sisisoso.wholesaleairtimes.app.api.requests.CheckPaymentRequest
import com.sisisoso.wholesaleairtimes.app.api.responses.CheckPaymentResponse
import com.sisisoso.wholesaleairtimes.app.api.responses.StkPushResponse
import com.sisisoso.wholesaleairtimes.app.models.AccessToken
import com.sisisoso.wholesaleairtimes.app.models.STKPush
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface RestApi {
    @get:GET("oauth/v1/generate?grant_type=client_credentials")
    val accessToken: Call<AccessToken>

    @POST("mpesa/stkpush/v1/processrequest")
    @Headers("Content-Type: application/json", "Accept: application/json")
    fun sendStkPush(@Body stkPush: STKPush): Call<StkPushResponse>

    @POST("mpesa/stkpushquery/v2/query")
    @Headers("Content-Type: application/json", "Accept: application/json")
    fun checkPayment(@Body checkPaymentRequest: CheckPaymentRequest): Call<CheckPaymentResponse>
}