package com.sisisoso.wholesaleairtimes.app.api.responses

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CheckPaymentResponse : Serializable {
    @SerializedName("ResponseCode")
    var ResponseCode: String? = null

    @SerializedName("ResultDesc")
    var ResultDesc: String? = null

    @SerializedName("CheckoutRequestID")
    var CheckoutRequestID: String? = null

    @SerializedName("MerchantRequestID")
    var MerchantRequestID: String? = null

    @SerializedName("MpesaReceiptNumber")
    var MpesaReceiptNumber: String? = null

    @SerializedName("ResponseDescription")
    var ResponseDescription: String? = null
}