package com.sisisoso.wholesaleairtimes.app.api.utils

/**
 *
 */
object AppConstants {
    const val READ_TIMEOUT = 60
    const val CONNECT_TIMEOUT = 60
    const val WRITE_TIMEOUT = 60
    const val BASE_URL = "https://sandbox.safaricom.co.ke/" // on live change to "https://api.safaricom.co.ke/"

    //STKPush Properties
    const val BUSINESS_SHORT_CODE = "SHORTCODE_HERE"
    const val PASSKEY = "PASS_KEY_HERE"
    const val TRANSACTION_TYPE = "CustomerBuyGoodsOnline"// if you are using paybill change it to "CustomerPayBillOnline"
    const val PARTYB = "8132004"// if you are using paybill change it to your shortcode
    const val CONSUMER_KEY = "CONSUMER_KEY_HERE"
    const val CONSUMER_SECRET = "CONSUMER_SECRET_HERE"
    const val CALLBACKURL = "CALL_BACK_URL_HERE"
}