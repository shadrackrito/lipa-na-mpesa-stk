package com.sisisoso.wholesaleairtimes

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.sisisoso.wholesaleairtimes.app.api.ApiClient
import com.sisisoso.wholesaleairtimes.app.api.requests.CheckPaymentRequest
import com.sisisoso.wholesaleairtimes.app.api.responses.CheckPaymentResponse
import com.sisisoso.wholesaleairtimes.app.api.responses.StkPushResponse
import com.sisisoso.wholesaleairtimes.app.api.utils.AppConstants.BUSINESS_SHORT_CODE
import com.sisisoso.wholesaleairtimes.app.api.utils.AppConstants.CALLBACKURL
import com.sisisoso.wholesaleairtimes.app.api.utils.AppConstants.PARTYB
import com.sisisoso.wholesaleairtimes.app.api.utils.AppConstants.PASSKEY
import com.sisisoso.wholesaleairtimes.app.api.utils.AppConstants.TRANSACTION_TYPE
import com.sisisoso.wholesaleairtimes.app.api.utils.TextValidator
import com.sisisoso.wholesaleairtimes.app.api.utils.Utils
import com.sisisoso.wholesaleairtimes.app.models.AccessToken
import com.sisisoso.wholesaleairtimes.app.models.STKPush
import com.sisisoso.wholesaleairtimes.databinding.ActivityMainBinding
import com.sisisoso.wholesaleairtimes.home.PaymentFalseDialogFragment
import com.sisisoso.wholesaleairtimes.home.PaymentTrueDialogFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private lateinit var b: ActivityMainBinding
    private lateinit var context: Context
    private var mAccountNo: String? = null
    private lateinit var checkingPayment: ProgressDialog
    private var flag = 0
    private lateinit var mApiClient: ApiClient
    private val ALLOW_SOUND = "allow_sound"
    private lateinit var mediaPlayer: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b = ActivityMainBinding.inflate(layoutInflater)
        setContentView(b.root)

        mApiClient = ApiClient()
        mApiClient.setIsDebug(true)

        val sharedPref: SharedPreferences = this.getSharedPreferences(
           ALLOW_SOUND,
            MODE_PRIVATE
        )


        accessToken

        b.txtPhoneNumber.addTextChangedListener(object : TextValidator() {
            override fun validate(s: String?) {
                if (s != null) {
                    b.tilPhoneNumber.error = null
                }
            }
        })

        b.txtAmount.addTextChangedListener(object : TextValidator() {
            override fun validate(s: String?) {
                if (s != null) {
                    b.tilAmount.error = null
                }
            }
        })

        context = this


        b.apply {

            b.tvPurchase.setOnClickListener {
                val timestamp: String = Utils.timestamp
                val mAmount =b.txtAmount.text.toString()
                val rawTelephone=b.txtPhoneNumber.text.toString()

                if (TextValidator.isEmpty(rawTelephone)) {
                    b.tilPhoneNumber.error = "This field is required"
                } else if (TextValidator.isEmpty(mAmount)) {
                    b.tilAmount.error =  "This field is required"
                }else{
                   if (rawTelephone.length == 10){
                       val mTelephone ="254"+rawTelephone.substring(1)
                       val mAccountNo ="WholeSale Airtimes"
                       val builder = android.app.AlertDialog.Builder(
                           context,
                           R.style.RoundedCornersDialog
                       )
                       val customView = layoutInflater.inflate(R.layout.custom_dialog, null)
                       builder.setView(customView)
                       val textView = customView.findViewById<TextView>(R.id.dialog_message)
                       textView.text ="Proceed to Buy Pay KES $mAmount With $rawTelephone to WholeSale Airtimes For Replacement SIM Cards?"

                       val alertDialog = builder.create()
                       builder.setCancelable(false)

                       val positiveButton = customView.findViewById<Button>(R.id.positive_button)
                       positiveButton.setOnClickListener { v: View? ->
                           val stkPush = STKPush(
                               BUSINESS_SHORT_CODE,
                               Utils.getPassword(BUSINESS_SHORT_CODE, PASSKEY, timestamp),
                               timestamp,
                               TRANSACTION_TYPE,
                               mAmount,
                               Utils.sanitizePhoneNumber(mTelephone),
                               PARTYB,
                               Utils.sanitizePhoneNumber(mTelephone),
                               CALLBACKURL,
                               mAccountNo,  //The account reference
                               "Load Airtime" //The transaction description
                           )

                           sendSTK(
                               stkPush, mTelephone, mAmount, mAccountNo,
                               Utils.getPassword(BUSINESS_SHORT_CODE, PASSKEY, timestamp), timestamp
                           )
                           alertDialog.dismiss()
                       }

                       val negativeButton = customView.findViewById<Button>(R.id.negative_button)
                       negativeButton.setOnClickListener { v: View? ->
                           // perform negative action
                           alertDialog.dismiss()
                       }

                       alertDialog.show()
                   }else{
                       Toast.makeText(context, "Start with 07... or 01...", Toast.LENGTH_SHORT).show()
                   }
                }



            }
        }


    }

    private fun sendSTK(
        stkPush: STKPush,
        telephone: String,
        amount: String,
        accountNo: String,
        password: String,
        timestamp: String
    ) {
        mAccountNo = accountNo
        val sendStkProgressDialog: ProgressDialog =
            ProgressDialog.show(context, "", "Initiating STK Push. Please wait...", true)
        mApiClient.setGetAccessToken(false)
        mApiClient.mpesaService().sendStkPush(stkPush).enqueue(object : Callback<StkPushResponse?> {
            override fun onResponse(
                call: Call<StkPushResponse?>,
                apiResponse: Response<StkPushResponse?>
            ) {
                sendStkProgressDialog.dismiss()
                try {
                    if (apiResponse.isSuccessful) {
                        val stkPushResponse: StkPushResponse? = apiResponse.body()
                        if (stkPushResponse != null) {
                            val CheckoutRequestID: String? = stkPushResponse.CheckoutRequestID
                            val MerchantRequestID: String? = stkPushResponse.MerchantRequestID
                            Toast.makeText(
                                context,
                                stkPushResponse.CustomerMessage,
                                Toast.LENGTH_SHORT
                            ).show()
                            val checkPaymentRequest = CheckPaymentRequest()
                            checkPaymentRequest.CheckoutRequestID = CheckoutRequestID
                            checkPaymentRequest.MerchantRequestID=MerchantRequestID
                            checkPaymentRequest.Password = password
                            checkPaymentRequest.BusinessShortCode =
                                BUSINESS_SHORT_CODE
                            checkPaymentRequest.Timestamp = timestamp
                            mCheckPayment(checkPaymentRequest, telephone, amount)
                        }
                    }
                    if (apiResponse.code() == 400) {
                        val snackbar: Snackbar = Snackbar
                            .make(
                                b.constraint,
                                "Please check your details and try again",
                                Snackbar.LENGTH_INDEFINITE
                            )
                            .setAction("Dismiss") { view: View? -> }
                        snackbar.show()

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<StkPushResponse?>, t: Throwable) {
                sendStkProgressDialog.dismiss()
            }
        })
    }

    private fun mCheckPayment(
        checkPaymentRequest: CheckPaymentRequest,
        telephone: String,
        amount: String
    ) {
        if (flag == 0) {
            flag = 1
            val progressDialog: ProgressDialog =
                ProgressDialog.show(this, "", "Checking Payment. Please wait...", true)
            mApiClient.setGetAccessToken(false)
            mApiClient.mpesaService().checkPayment(checkPaymentRequest)
                .enqueue(object : Callback<CheckPaymentResponse?> {
                    override fun onResponse(
                        call: Call<CheckPaymentResponse?>,
                        apiResponse: Response<CheckPaymentResponse?>
                    ) {
                        progressDialog.dismiss()
                        if (apiResponse.isSuccessful) {
                            val checkPaymentResponse: CheckPaymentResponse? = apiResponse.body()
                            if (checkPaymentResponse != null) {
                                if (checkPaymentResponse.ResponseCode.equals("4002")) {
                                    checkingPayment = ProgressDialog.show(
                                        context,
                                        "",
                                        "Confirming Payment. Please wait...",
                                        true
                                    )
                                    mCheckPayment(checkPaymentRequest, telephone, amount)
                                } else if (checkPaymentResponse.ResponseCode.equals("0")) {
                                    checkingPayment.dismiss()
                                    if (!checkPaymentResponse.MpesaReceiptNumber.equals("")) {
                                        checkPaymentResponse.MpesaReceiptNumber?.let {
                                            PaymentTrueDialogFragment.newInstance(
                                                it
                                            )
                                        }?.show(supportFragmentManager, "MyDialogFragment")

                                        mediaPlayer =
                                            MediaPlayer.create(context, R.raw.valid_ticket)
                                        mediaPlayer.start()

                                    } else {
                                        val imm =
                                            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                                        imm.hideSoftInputFromWindow(
                                            b.constraint.windowToken, 0
                                        )

                                        checkPaymentResponse.ResultDesc?.let {
                                            PaymentFalseDialogFragment.newInstance(
                                                it
                                            )
                                        }?.show(supportFragmentManager, "MyDialogFragment")

                                        mediaPlayer =
                                            MediaPlayer.create(context, R.raw.invalid_ticket)
                                        mediaPlayer.start()


                                        checkingPayment.dismiss()
                                    }
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<CheckPaymentResponse?>, t: Throwable) {
                        progressDialog.dismiss()
                    }
                })
        } else if (flag == 1) {
            mApiClient.setGetAccessToken(false)
            mApiClient.mpesaService().checkPayment(checkPaymentRequest)
                .enqueue(object : Callback<CheckPaymentResponse?> {
                    override fun onResponse(
                        call: Call<CheckPaymentResponse?>,
                        apiResponse: Response<CheckPaymentResponse?>
                    ) {
                        if (apiResponse.isSuccessful) {
                            val checkPaymentResponse: CheckPaymentResponse? = apiResponse.body()
                            if (checkPaymentResponse != null) {
                                if (checkPaymentResponse.ResponseCode.equals("4002")) {
                                    mCheckPayment(checkPaymentRequest, telephone, amount)
                                } else if (checkPaymentResponse.ResponseCode.equals("0")) {
                                    checkingPayment.dismiss()
                                    if (!checkPaymentResponse.MpesaReceiptNumber.equals("")) {
                                        checkPaymentResponse.MpesaReceiptNumber?.let {
                                            PaymentTrueDialogFragment.newInstance(
                                                it
                                            )
                                        }?.show(supportFragmentManager, "MyDialogFragment")

                                        mediaPlayer =
                                            MediaPlayer.create(context, R.raw.valid_ticket)
                                        mediaPlayer.start()
                                    } else {
                                        val imm =
                                            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                                        imm.hideSoftInputFromWindow(
                                            b.root.windowToken, 0
                                        )
                                        checkPaymentResponse.ResultDesc?.let {
                                            PaymentFalseDialogFragment.newInstance(
                                                it
                                            )
                                        }?.show(supportFragmentManager, "MyDialogFragment")
                                        mediaPlayer =
                                            MediaPlayer.create(context, R.raw.invalid_ticket)
                                        mediaPlayer.start()

                                        checkingPayment.dismiss()
                                    }
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<CheckPaymentResponse?>, t: Throwable) {
                        checkingPayment.dismiss()
                    }
                })
        }
    }


    private val accessToken: Unit
        private get() {
            mApiClient.setGetAccessToken(true)
            mApiClient.mpesaService().accessToken.enqueue(object : Callback<AccessToken> {
                override fun onResponse(call: Call<AccessToken>, response: Response<AccessToken>) {
                    if (response.isSuccessful) {
                        response.body()?.let { mApiClient.setAuthToken(it.accessToken) }
                    }
                }

                override fun onFailure(call: Call<AccessToken>, t: Throwable) {}
            })
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 124 && resultCode == Activity.RESULT_OK && data != null) {
            b.txtAmount.text = null
            b.txtPhoneNumber.text = null
        }
    }

}