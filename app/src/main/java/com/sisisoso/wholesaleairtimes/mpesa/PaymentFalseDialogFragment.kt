package com.sisisoso.wholesaleairtimes.home

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.sisisoso.wholesaleairtimes.R
import com.sisisoso.wholesaleairtimes.databinding.FragmentPaymentFalseDialogBinding
import com.sisisoso.wholesaleairtimes.databinding.FragmentPaymentTrueDialogBinding

class PaymentFalseDialogFragment : DialogFragment() {

    lateinit var mpesaCode:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL,  R.style.RoundedCornersDialog)
        isCancelable = false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val binding: FragmentPaymentFalseDialogBinding = FragmentPaymentFalseDialogBinding.inflate(
            layoutInflater
        )

        val mpesaCode = arguments?.getString(ARG_TITLE)

        binding.tvPaymentDetail.text = mpesaCode


        binding.btnDone.setOnClickListener {
            val result = Intent()
            result.putExtra("success", true)
            parentFragment?.onActivityResult(124, Activity.RESULT_OK, result)
            dismiss()
        }
        return binding.root
    }

    companion object {
        private const val ARG_TITLE = "mpesa_code"
        fun newInstance(mpesa_code: String): PaymentFalseDialogFragment {
            val fragment = PaymentFalseDialogFragment()
            val args = Bundle().apply {
                putString(ARG_TITLE, mpesa_code)
            }
            fragment.arguments = args
            return fragment
        }

    }


}